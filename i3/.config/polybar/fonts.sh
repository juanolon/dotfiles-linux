# export BAT_EMPTY="$(echo -e "\uebdc")"
# export BAT1="$(echo -e "\uebd9")"
# export BAT2="$(echo -e "\uebe0")"
# export BAT3="$(echo -e "\uebdd")"
# export BAT4="$(echo -e "\uebe2")"
# export BAT5="$(echo -e "\uebd4")"
# export BAT6="$(echo -e "\uebd2")"
# export BAT_FULL="$(echo -e "\ue1a4")"
# export BAT_CRIT="$(echo -e "\ue19c")"
# export BAT_CHARG="$(echo -e "\ue1a3")"

export BAT_EMPTY="$(echo -e "\UF08E")"
export BAT1="$(echo -e "\UF07A")"
export BAT2="$(echo -e "\UF07B")"
export BAT3="$(echo -e "\UF07C")"
export BAT4="$(echo -e "\UF07D")"
export BAT5="$(echo -e "\UF07E")"
export BAT6="$(echo -e "\UF080")"
export BAT7="$(echo -e "\UF081")"
export BAT8="$(echo -e "\UF082")"
export BAT_FULL="$(echo -e "\UF079")"
export BAT_CRIT="$(echo -e "\UF083")"
export BAT_CHARG="$(echo -e "\UF084")"

export CHARG_BAT1="$(echo -e "\UF086")"
export CHARG_BAT2="$(echo -e "\UF087")"
export CHARG_BAT3="$(echo -e "\UF088")"
export CHARG_BAT4="$(echo -e "\UF089")"
export CHARG_BAT5="$(echo -e "\UF08A")"
export CHARG_BAT6="$(echo -e "\UF08B")"
export CHARG_FULL="$(echo -e "\UF085")"

export WIFI_EMPTY="$(echo -e "\uf0b0")"

export WIFI_FULL_WAR="$(echo -e "\UE8A9")"
export WIFI_STRIP_OFF="$(echo -e "\UE648")"
export WIFI_STRIP="$(echo -e "\UE63E")"

export WIFI_FULL="$(echo -e "\UE1D8")"
export WIFI_OFF="$(echo -e "\UE1DA")"

export LAN="$(echo -e "\ueb2f")"
export AIRPLANE="$(echo -e "\ue195")"

export KEYBOARD="$(echo -e "\ue312")"

# see touchpad script
export NO_TOUCH="$(echo -e '\uf1b0')"
export TOUCH="$(echo -e '\ue913')"

# see rofi-bluetooth script
export BT_ON="$(echo -e '\ue1a7')"
export BT_OFF="$(echo -e '\ue1a9')"
export BT_CON="$(echo -e '\ue1a8')"

export CLOCK="$(echo -e '\ue924')"
export CPU="$(echo -e '\ue322')"
export MEM="$(echo -e '\ue1db')"
export USB="$(echo -e '\UE1E0')"

export VERT_EMPTY="$(echo -e '\U2593')"
export VERT_FULL="$(echo -e '\U2589')"

export HOR1="$(echo -e '\U2581')"
export HOR2="$(echo -e '\U2582')"
export HOR3="$(echo -e '\U2583')"
export HOR4="$(echo -e '\U2584')"
export HOR5="$(echo -e '\U2585')"
export HOR6="$(echo -e '\U2586')"
export HOR7="$(echo -e '\U2587')"
export HOR6="$(echo -e '\U2588')"
