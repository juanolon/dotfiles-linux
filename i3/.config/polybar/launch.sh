#!/usr/bin/env bash

BASE="$(dirname "$0")"
ABS=$(cd "$BASE" && pwd)
echo "$ABS/fonts.sh"
# set -o allexport; set -e
. "$ABS/fonts.sh"
# set +o allexport; set +e

DEFAULT="eDP-1"

CONNECTED=$(xrandr --listactivemonitors | grep "^ [0-9]" | cut -d ' ' -f 3 | sed 's/[*|+]*//')
CHILD=$(echo "$CONNECTED" | grep -v $DEFAULT)

help() {
    cat <<EOF
    Usage: $0 <command> [<args>]

    Available commands:
        init <monitor>      Will terminate any bar, and initialize the main configuration. If monitor is given, then monitor has the main bar.
        attach <monitor>    Attach secondary bar to <monitor>.
        deattach            Terminate all bars except the main bar.
EOF
exit 1
}

case $1 in
    init)
        killall -q polybar

        if [ -n "$2" ]; then
            echo "Overriding DEFAULT monitor with $2"
            DEFAULT=$2
            CHILD=$(echo "$CONNECTED" | grep -v $DEFAULT)
        fi
        echo "initializing main bar on $DEFAULT"
        MONITOR=$DEFAULT polybar top-bar-main &

        if [ -n "$CHILD" ]; then
            for MONITOR_NAME in $CHILD
            do
                echo "enabling secondary bar on $MONITOR_NAME"
                MONITOR=$MONITOR_NAME polybar top-bar-secondary &
            done
        fi
        ;;
    detach)
        TODETACH=$(pgrep polybar -a | grep 'top-bar-secondary' | cut -d ' ' -f1)
        if [ -n "$TODETACH" ]; then
            for ID in $TODETACH
            do
                echo "killing bar PID: $ID"
                kill $ID
            done
        fi
        ;;
    attach)
        if [ -n "$2" ]; then
            if $CONNECTED | grep -q "$2"
            then
                echo "Monitor $2 doesn' exist"
                exit 1
            fi
            echo "attaching secondary bar on $2"
            # TODO check if monitor exists
            MONITOR=$2 polybar top-bar-secondary &
        else
            echo "use: $0 attach <monitor>"
            exit 1
        fi
        ;;
    *)
        help
    ;;
esac
