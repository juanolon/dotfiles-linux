#!/usr/bin/env bash
#
# Intended to be run by astroid. See the following link for more information:
# https://github.com/astroidmail/astroid/wiki/Polling

# Exit as soon as one of the commands fail.
set -e

# Fetch new mail.
# mbsync -qq mail
mbsync -qq uni-inbox

# Import new mail into the notmuch database.
notmuch new

# categorize mails
afew --tag --new
