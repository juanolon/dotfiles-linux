#!/bin/bash

status=$(playerctl -p spotify status)

if [[ -z $status ]] 
then
   # spotify is dead, we should die to.
   exit
fi

artist=$(playerctl -p spotify metadata xesam:artist | sed 's/&/\&amp;/g')
title=$(playerctl -p spotify metadata xesam:title | sed 's/&/\&amp;/g')
album=$(playerctl -p spotify metadata xesam:album | sed 's/&/\&amp;/g')

class=$(echo $status | tr '[:upper:]' '[:lower:]')


case $1 in
    play-pause)
            echo "{\"alt\": \"$class\"}"
        ;;
    current-song)
        echo "{\"class\": \"$class\", \"text\": \"$artist - $title\", \"tooltip\": \"$artist - $title - $album\"}"
        ;;
        *)
        echo "Use: status.sh play-pause|current-song"
        ;;
esac
