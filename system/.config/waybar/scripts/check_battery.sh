#!/bin/bash

# $1 BAT0/BAT1..
# $2 critical level

bat=/sys/class/power_supply/$1
CRIT=${2:-25}

FILE=/tmp/battery_notified

stat=$(cat $bat/status)
perc=$(cat $bat/capacity)

if [[ $perc -le $CRIT ]] && [[ $stat == "Discharging" ]]; then
    notify-send --urgency=critical --icon="battery-caution-symbolic" "Battery Low on $1" "Current charge: $perc%"
fi

# if [[ $perc -le $CRIT ]] && [[ $stat == "Discharging" ]]; then
#     if [[ ! -f "$FILE" ]]; then
#         notify-send --urgency=critical --icon=dialog-warning "Battery Low" "Current charge: $perc%"
#         touch $FILE
#     fi
# elif [[ -f "$FILE" ]]; then
#     echo "del"
#     rm $FILE
# fi
