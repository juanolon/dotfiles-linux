return {
    {
        'goerz/jupytext.vim',
        ft = "python",
        init = function ()
            vim.g['jupytext_fmt'] = 'py:percent' -- custom s:jupytext_extension_map
            vim.g['jupytext_to_ipynb_opts'] = '--to=py:percent'
            vim.g['jupytext_filetype_map'] = "{'md': 'tex'}"

            vim.g['jupytext_print_debug_msgs'] = 0
        end
    }
}
