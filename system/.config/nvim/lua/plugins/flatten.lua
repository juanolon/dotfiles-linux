return {
    {
        "willothy/flatten.nvim",
        enabled = false,
        opts = {
            window = {
                open = "tab"
            },
            one_per = {
                kitty = true
            }
        },
        -- Ensure that it runs first to minimize delay when opening file from terminal
        lazy = false,
        priority = 1001,
    },
}
