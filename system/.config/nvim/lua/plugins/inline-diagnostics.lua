local colors = require("rose-pine.palette")

return {
    {
        "rachartier/tiny-inline-diagnostic.nvim",
        event = "VeryLazy",
        opts = {
            signs = {
                left = "",
                right = ""
            },
            hi = {
                background = colors.base,
                mixing_color = colors.base
            },
        }
    }
}
