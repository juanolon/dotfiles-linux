return {
    {
        'neovim/nvim-lspconfig',
        event = { 'BufReadPre', 'BufNewFile' },
        dependencies = {
            'williamboman/mason-lspconfig.nvim',
            {
                'williamboman/mason.nvim',
                cmd = 'Mason',
                build = ':MasonUpdate',
                opts = {
                    ui = {
                        border = 'rounded',
                        width = 0.7,
                        height = 0.8,
                    },
                },
            },
        },
        config = function()
            local lspconfig = require 'lspconfig'
            local function capabilities()
                return vim.tbl_deep_extend(
                    'force',
                    vim.lsp.protocol.make_client_capabilities(),
                    -- nvim-cmp supports additional completion capabilities, so broadcast that to servers.
                    require('cmp_nvim_lsp').default_capabilities()
                )
            end

            require('lspconfig.ui.windows').default_options.border = 'rounded'

            require('mason-lspconfig').setup {
                ensure_installed = {
                    'bashls',
                    'eslint',
                    'jsonls',
                    'lua_ls',
                    -- css modules
                    'cssmodules_ls',
                    'cssls',
                    -- go
                    'gopls',
                    -- python
                    'pyright',
                    -- 'mypy',
                    -- 'black',
                    'ruff_lsp',
                    -- rust
                    'rust_analyzer',
                    -- latex
                    'texlab',
                    'ltex',   -- language tool grammar/spelling erros for latex/markdown
                    -- notes
                    'zk',
                    -- linting
                    -- 'efm',
                },
                handlers = {
                    function(server)
                        lspconfig[server].setup { capabilities = capabilities() }
                    end,
                    pyright = function()
                        lspconfig.pyright.setup {
                            settings = {
                                pyright = {
                                    -- Using Ruff's import organizer
                                    disableOrganizeImports = true,
                                },
                                python = {
                                    analysis = {
                                        ignore = { '*' },
                                    },
                                },
                            },
                        }
                    end,
                    texlab = function()
                        lspconfig.texlab.setup {
                            forwardSearch = {
                                executable = "zathura",
                                args = { "--synctex-forward", "%l:1:%f", "%p" },
                                onSave = true,
                            },
                            build = {
                                forwardSearchAfter = true
                            }
                        }
                    end,
                    -- zk = function()
                    --     lspconfig.zk.setup {
                    --         default_config = {
                    --             cmd = { 'zk', 'lsp' },
                    --             filetypes = { 'markdown' },
                    --             root_dir = function()
                    --                 return lspconfig.util.root_pattern('.zk')
                    --             end,
                    --             settings = {}
                    --         }
                    --     }
                    -- end,
                    tsserver = function()
                        lspconfig.tsserver.setup {
                            filetypes = {
                                "typescript", "typescriptreact", "typescript.tsx"
                            },
                            cmd = { "typescript-language-server", "--stdio" }
                        }
                    end,
                    lua_ls = function ()
                        lspconfig.lua_ls.setup {
                            capabilities = capabilities(),
                            on_init = function(client)
                                local path = client.workspace_folders
                                and client.workspace_folders[1]
                                and client.workspace_folders[1].name
                                if
                                    not path
                                    or not (
                                    vim.uv.fs_stat(path .. '/.luarc.json')
                                    or vim.uv.fs_stat(path .. '/.luarc.jsonc')
                                    )
                                    then
                                        client.config.settings = vim.tbl_deep_extend('force', client.config.settings, {
                                            Lua = {
                                                runtime = {
                                                    version = 'LuaJIT',
                                                },
                                                workspace = {
                                                    checkThirdParty = false,
                                                    library = {
                                                        vim.env.VIMRUNTIME,
                                                        '${3rd}/luv/library',
                                                    },
                                                },
                                            },
                                        })
                                        client.notify(
                                        vim.lsp.protocol.Methods.workspace_didChangeConfiguration,
                                        { settings = client.config.settings }
                                        )
                                    end

                                    return true
                                end,
                                settings = {
                                    Lua = {
                                        -- Using stylua for formatting.
                                        format = { enable = false },
                                        hint = {
                                            enable = true,
                                            arrayIndex = 'Disable',
                                        },
                                        completion = { callSnippet = 'Replace' },
                                    },
                                },
                            }
                    end,
                    jsonls = function()
                        lspconfig.jsonls.setup {
                            capabilities = capabilities(),
                            settings = {
                                json = {
                                    validate = { enable = true },
                                    format = { enable = true },
                                },
                            },
                            -- Lazy-load schemas.
                            on_new_config = function(config)
                                config.settings.json.schemas = config.settings.json.schemas or {}
                                vim.list_extend(config.settings.json.schemas, require('schemastore').json.schemas())
                            end,
                        }
                    end,
                    -- efm = function ()
                    --     local languages = {
                    --         lua = {
                    --             require('efmls-configs.formatters.stylua'),
                    --         },
                    --         python = {
                    --             -- using ruff_lsp
                    --             -- require('efmls-configs.formatters.ruff'),
                    --             -- require('efmls-configs.linters.ruff'),
                    --             require('efmls-configs.linters.mypy')
                    --         }
                    --     }
                    --
                    --     lspconfig.efm.setup {
                    --         filetypes = vim.tbl_keys(languages),
                    --         settings = {
                    --             rootMarkers = { '.git/' },
                    --             languages = languages,
                    --         },
                    --         init_options = {
                    --             documentFormatting = true,
                    --             documentRangeFormatting = true,
                    --         },
                    --     }
                    -- end
                },
            }
        end,
    },
}
