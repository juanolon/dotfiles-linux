return {
    {
        'opdavies/toggle-checkbox.nvim',
        event = "VeryLazy",
        keys = {
            {
                -- Customize or remove this keymap to your liking
                "<leader>tt",
                ":lua require('toggle-checkbox').toggle()<CR>",
                desc = "Toggle checkbox",
            },
        },
    }
}
