return {
    'HakonHarnes/img-clip.nvim',
    event = "VeryLazy",
    ft = { "markdown", "latex" },
    opts = {
        dir_path = '.',
        relative_to_current_file = true,
    },
    keys = {
        { "<leader>p", "<cmd>PasteImage<cr>", desc = "Paste image from system clipboard" },
    },
}
