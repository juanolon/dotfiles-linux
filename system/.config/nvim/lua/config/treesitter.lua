require("nvim-treesitter.configs").setup ({
    highlight = {
        enable = true,
        disable = { 'latex', 'bibtex' },
        -- additional_vim_regex_highlighting = { "latex", "markdown" },
    },
    indent = {
        enable = true,
        disable = {},
    },
    ignore_install = { "latex" },
    ensure_installed = {
        "tsx",
        "json",
        "yaml",
        "css",
        "html",
        "lua",
        "python",
        "markdown",
        "markdown_inline",
        "typescript",
        "vimdoc",
        "go",
        "bash",
        "vim",
        "gitignore",
        "query",
        "dart",
        "javascript",
        "typescript",
        "latex",
        "comment"
    },
    auto_install = false,
    -- disable on large files
    disable = function(lang, buf)
        local max_filesize = 100 * 1024 -- 100 KB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
    end,
    autotag = {
        enable = false,
    },
})

local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.tsx.filetype_to_parsername = { "javascript", "typescript.tsx" }
