local colors = require("rose-pine.palette")
local theme = require('lualine.themes.rose-pine')

-- theme.normal.c.bg = colors.overlay
-- theme.insert.c.bg = colors.overlay
-- theme.visual.c.bg = colors.overlay
-- theme.replace.c.bg = colors.overlay
-- theme.command.c.bg = colors.overlay
-- theme.inactive.c.bg = colors.overlay
local pine = {
    normal = {
        a = { bg = colors.overlay, fg = colors.rose, gui = "bold" },
        b = { bg = colors.overlay, fg = colors.rose },
        c = { bg = colors.base, fg = colors.text },
    },
    insert = {
        a = { bg = colors.overlay, fg = colors.foam, gui = "bold" },
        b = { bg = colors.overlay, fg = colors.foam },
        c = { bg = colors.base, fg = colors.text },
    },
    visual = {
        a = { bg = colors.overlay, fg = colors.iris, gui = "bold" },
        b = { bg = colors.overlay, fg = colors.iris },
        c = { bg = colors.base, fg = colors.text },
    },
    replace = {
        a = { bg = colors.overlay, fg = colors.pine, gui = "bold" },
        b = { bg = colors.overlay, fg = colors.pine },
        c = { bg = colors.base, fg = colors.text },
    },
    command = {
        a = { bg = colors.overlay, fg = colors.love, gui = "bold" },
        b = { bg = colors.overlay, fg = colors.love },
        c = { bg = colors.base, fg = colors.text },
    },
    inactive = {
        a = { bg = colors.overlay, fg = colors.muted, gui = "bold" },
        b = { bg = colors.overlay, fg = colors.muted },
        c = { bg = colors.base, fg = colors.muted },
    },
}

-- ○ ◌ ◍ ◎ ● ◉ ◐ ◑ ◒ ◓ ◔ ◕
-- ⧎ ⧏ ⧐ ⧑ ⧒ ⧓ ⧔ ⧕ ⧖ ⧗
-- ※ ⁂ ⁑ ⁖ ⁘ ⁙ ⁚ ⁛ ⁜
--ᗄ ᗅ ᗆ ᗇ ᗈ ᗉ ᗊ ᗋ ᗌ ᗍ ᗎ ᗏ ᗐ ᗑ ᗒ ᗓ ᗔ ᗕ ᗖ ᗗ ᗘ ᗙ ᗚ ᗛ
--⊕ ⊖ ⊗ ⊘ ⊙ ⊚ ⊛ ⊜  ⊝
local mode = {
    -- normal
    ['n']      = { color = colors.pine, icon = "⊚"},
    ['niI']    = { color = colors.pine, icon = "⊚"},
    ['niR']    = { color = colors.pine, icon = "⊚"},
    ['niV']    = { color = colors.pine, icon = "⊚"},
    ['nt']     = { color = colors.pine, icon = "⊚"},
    ['ntT']    = { color = colors.pine, icon = "⊚"},
    -- o-pending
    ['no']     = { color = colors.pine, icon = "⊚"},
    ['nov']    = { color = colors.pine, icon = "⊚"},
    ['noV']    = { color = colors.pine, icon = "⊚"},
    ['no\22']  = { color = colors.pine, icon = "⊚"},
    -- visual
    ['v']      = { color = colors.iris, icon = "⊙"},
    ['vs']     = { color = colors.iris, icon = "⊙"},
    ['V']      = { color = colors.iris, icon = "⊙"},
    ['Vs']     = { color = colors.iris, icon = "⊙"},
    ['\22']    = { color = colors.iris, icon = "⊙"},
    ['\22s']   = { color = colors.iris, icon = "⊙"},
    -- insert
    ['i']      = { color = colors.love, icon = "⊕"},
    ['ic']     = { color = colors.love, icon = "⊕"},
    ['ix']     = { color = colors.love, icon = "⊕"},
    -- select
    ['s']      = { color = colors.rose, icon = "⊛"},
    ['S']      = { color = colors.rose, icon = "⊛"},
    ['\19']    = { color = colors.rose, icon = "⊛"},
    -- replace
    ['R']      = { color = colors.love, icon = "⊖"},
    ['Rc']     = { color = colors.love, icon = "⊖"},
    ['Rx']     = { color = colors.love, icon = "⊖"},
    ['Rv']     = { color = colors.love, icon = "⊖"},
    ['Rvc']    = { color = colors.love, icon = "⊖"},
    ['Rvx']    = { color = colors.love, icon = "⊖"},
    ['r']      = { color = colors.love, icon = "⊖"},
    -- command
    ['c']      = { color = colors.gold, icon = "⊜"},
    ['cv']     = { color = colors.gold, icon = "⊜"},
    ['ce']     = { color = colors.gold, icon = "⊜"},
    -- more
    ['rm']     = { color = colors.gold, icon = "⊜"},
    -- confirm
    ['r?']     = { color = colors.gold, icon = "⊜"},
    -- shell
    ['!']      = { color = colors.gold, icon = "⊜"},
    -- terminal
    ['t']      = { color = colors.gold, icon = "⊜"},
}

--- @param trunc_width number trunctates component when screen width is less then trunc_width
--- @param trunc_len number truncates component to trunc_len number of chars
--- @param hide_width number hides component when window width is smaller then hide_width
--- @param no_ellipsis boolean whether to disable adding '...' at end after truncation
--- return function that can format the component accordingly
local function trunc(trunc_width, trunc_len, hide_width, no_ellipsis)
  return function(str)
    local win_width = vim.fn.winwidth(0)
    if hide_width and win_width < hide_width then return ''
    elseif trunc_width and trunc_len and win_width < trunc_width and #str > trunc_len then
       return str:sub(1, trunc_len) .. (no_ellipsis and '' or '...')
    end
    return str
  end
end
-- require'lualine'.setup {
--   lualine_a = {
--     {'mode', fmt=trunc(80, 4, nil, true)},
--     {'filename', fmt=trunc(90, 30, 50)},
--     {function() return require'lsp-status'.status() end, fmt=trunc(120, 20, 60)}
--   }
-- }

require'lualine'.setup {
	options = {
	    icons_enabled = true,
	    theme = pine,
	    -- component_separators = {'', ''},
	    -- section_separators = {'', ''},
	    component_separators = '',
        -- section_separators = { left = '|', right = '|' },
	    -- section_separators = {left = '▙', right = '▟'},
	    section_separators = {left = '', right = ''},
	    disabled_filetypes = {}
	},
	sections = {
	    -- lualine_a = {{'mode'}},
	    lualine_a = {
            -- Colored mode icon
            {
                function() return mode[vim.fn.mode()]['icon'] end,
                -- color = function() return { fg = mode[vim.fn.mode()]['color'] } end,
                padding = { right = 1, left = 1 },
            }
        },
        -- lualine_b = {'diff'},
        -- lualine_b = { 'branch', 'diff',
        --     {
        --         'diagnostics',
        --         sources = { "nvim_diagnostic", "nvim_lsp" },
        --         sections = { 'error', 'warn' },
        --         symbols = { error = ' ', warn = ' ', info = ' ', hint = ' ' }
        --     }
        -- },
	    lualine_b = {'filename'},
	    lualine_c = {},
	    lualine_x = {
	            'lsp_progress',
	        'diff',
	        { 'copilot', 
            symbols = {
                status = {
                    icons = {
                        enabled = "", -- 
                        sleep = "",   -- auto-trigger disabled
                        disabled = " ", -- 
                        warning = " ", -- copilot returns an error
                        unknown = "" -- copilot is not attached/loaded
                    },
                    hl = {
                        enabled = colors.foam,
                        sleep = colors.subtle,
                        disabled = colors.muted,
                        warning = colors.love,
                        unknown = colors.love
                    }
                },
                -- spinners = require("copilot-lualine.spinners").dots,
                -- spinner_color = colors.text
            },
            -- show_colors = true,
            -- show_loading = true
	        },
	        -- 'encoding', 
        },
	    lualine_y = {
	        {
	            'fileformat',
	            symbols = {
                    unix = '',
                    dos = '',
                    mac = ''
                }
            },
	        'filetype'
	    },
	    lualine_z = {'progress', 'location'}
	},
	inactive_sections = {
	    lualine_a = {},
	    lualine_b = {},
	    lualine_c = {'filename'},
	    lualine_x = {'location'},
	    lualine_y = {},
	    lualine_z = {}
	},
	tabline = {},
	extensions = {}
}
