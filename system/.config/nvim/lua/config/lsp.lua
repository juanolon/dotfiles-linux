local lspconfig = require('lspconfig')

require("mason").setup()

-- CONFIGURE LSP
local lsp = vim.lsp
local handlers = lsp.handlers

-- Hover doc popup
local pop_opts = { border = "rounded", max_width = 80 }
handlers["textDocument/hover"] = lsp.with(handlers.hover, pop_opts)
handlers["textDocument/signatureHelp"] = lsp.with(handlers.signature_help, pop_opts)

vim.diagnostic.config({
  -- virtual_text = {
  --   prefix = '⠿', -- Could be '●', '▎', 'x'
  -- },
  virtual_text = false,
  severity_sort = true,
  float = {
    border = 'rounded',
    source = 'always',
  },
})

-- icons
local signs = { Error = "", Warn = "", Hint = "", Info = "" }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end


-- INSTALL LSP SERVERS
-- list servers
local servers = {
    'texlab',   -- latex
    'pyright',  -- python
    'ltex',     -- language tool grammar/spelling erros for latex/markdown
    'gopls',    -- go
    'tsserver', -- typescript
    'cssmodules_ls',    -- css modules
    'black',
    'mypy',
    'ruff',
    -- 'rome',      -- js/ts linter/formatter
    'rust_analyzer'
}

-- custom server configuration
local custom_opts = {
    ["texlab"] = function(opts)
        opts.forwardSearch = {
            executable = "zathura",
            args = {"--synctex-forward", "%l:1:%f", "%p"},
            onSave = true;
        }
        opts.build = {
            forwardSearchAfter = true
        }
    end,
    ["tsserver"] = function(opts)
        opts.filetypes = {
            "typescript", "typescriptreact", "typescript.tsx"
        }
        opts.cmd = { "typescript-language-server", "--stdio" }
    end,
}

-- buffer mapping
local on_attach = function(client, bufnr)
    -- Set some keybinds conditional on server capabilities
    if client.server_capabilities.document_formatting then
        vim.keymap.set("n", "<space>f", vim.lsp.buf.formatting, bopts)
    elseif client.server_capabilities.document_range_formatting then
        vim.keymap.set("n", "<space>f", vim.lsp.buf.range_formatting, bopts)
    end

    -- Set autocommands conditional on server_capabilities
    if client.server_capabilities.document_highlight then
        vim.api.nvim_exec([[
        augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
        augroup END
        ]], false)
    end

    -- lsp signature
    -- require "lsp_signature".on_attach({
    --     bind = true,
    --     fix_pos = true,
    --     handler_opts = {
    --         border = "single"
    --     }
    -- })
end

-- config that activates keymaps and enables snippet support
-- local capabilities = vim.lsp.protocol.make_client_capabilities()
-- -- this may be only needed for nvim-compe
-- capabilities.textDocument.completion.completionItem.snippetSupport = true
-- capabilities.textDocument.completion.completionItem.resolveSupport = {
--     properties = {"documentation", "detail", "additionalTextEdits"}
-- }


local capabilities = require('cmp_nvim_lsp').default_capabilities()

-- configure servers
-- default config
local opts = {
    on_attach = on_attach,
    capabilities = capabilities,
}

-- configure zk
local configs = require('lspconfig/configs')

configs.zk = {
  default_config = {
    cmd = {'zk', 'lsp'},
    filetypes = {'markdown'},
    root_dir = function()
      return lspconfig.util.root_pattern('.zk')
    end,
    settings = {}
  };
}

-- require 'ltex-ls'.setup {
--     on_attach = on_attach,
--     capabilities = capabilities,
--     use_spellfile = false,
--     filetypes = { "latex", "tex", "bib", "markdown", "gitcommit", "text" },
--     settings = {
--         ltex = {
--             enabled = { "latex", "tex", "bib", "markdown", },
--             language = "auto",
--             diagnosticSeverity = "information",
--             sentenceCacheSize = 2000,
--             additionalRules = {
--                 enablePickyRules = true,
--                 motherTongue = "en",
--             },
--             disabledRules = {
--             },
--             dictionary = (function()
--                 -- For dictionary, search for files in the runtime to have
--                 -- and include them as externals the format for them is
--                 -- dict/{LANG}.txt
--                 --
--                 -- Also add dict/default.txt to all of them
--                 local files = {}
--                 for _, file in ipairs(vim.api.nvim_get_runtime_file("dict/*", true)) do
--                     local lang = vim.fn.fnamemodify(file, ":t:r")
--                     local fullpath = vim.fs.normalize(file, ":p")
--                     files[lang] = { ":" .. fullpath }
--                 end

--                 if files.default then
--                     for lang, _ in pairs(files) do
--                         if lang ~= "default" then
--                             vim.list_extend(files[lang], files.default)
--                         end
--                     end
--                     files.default = nil
--                 end
--                 return files
--             end)(),
--         },
--     },
-- }


local setup_servers = vim.list_extend(servers, {
    "zk",
    })
-- for _, server_name in ipairs(setup_servers) do
for _, server_name in ipairs(servers) do
    -- install server if not installed
    -- local server_is_found, server = lsp_installer.get_server(server_name)
    -- if server_is_found and not server:is_installed() then
    --     print("Installing " .. server_name)
    --     server:install()
    -- end


    if custom_opts[server_name] then
        custom_opts[server_name](opts)
    end

    -- configure!
    -- print("configuring " .. server_name)
    lspconfig[server_name].setup(opts)
end

-- lspconfig['dartls'].setup(opts)

-- Automatically reload after `:LspInstall <server>` so we don't have to restart neovim
--[[ require'lspinstall'.post_install_hook = function ()
setup_servers() -- reload installed servers
vim.cmd("bufdo e") -- this triggers the FileType autocmd that starts the server
end ]]
-- autocmd CursorHold * lua vim.lsp.diagnostic.show_line_diagnostics()
-- vim.cmd [[autocmd CursorHoldI * silent! lua vim.lsp.buf.signature_help()]]
