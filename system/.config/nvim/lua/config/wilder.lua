local wilder = require('wilder')

wilder.setup({
    modes = {':', '/', '?'},
    next_key = '<Tab>',
    previous_key = '<S-Tab>',
    -- accept_key = '<Enter>',
    -- reject_key = '<Esc>',
    -- enable_cmdline_enter = 0
})

wilder.set_option('pipeline', {
  wilder.branch(
    wilder.python_file_finder_pipeline({
      -- file_command = {'rg', '-i', '--files'}, 
      -- dir_command = {'fdfind', '-tf'},
      file_command = function(ctx, arg)
        if string.find(arg, '.') ~= nil then
          return {'fdfind', '-tf', '-H'}
        else
          return {'fdfind', '-tf'}
        end
      end,
      dir_command = {'fdfind', '-td'},
      -- filters = { 'cpsm_filter' },
      filters = { 'fuzzy_filter' },
    }),

    wilder.substitute_pipeline({
      pipeline = wilder.python_search_pipeline({
        skip_cmdtype_check = 1,
        pattern = wilder.python_fuzzy_pattern({
          start_at_boundary = 0,
        }),
      }),
    }),

    wilder.cmdline_pipeline({
        language = 'python',
        -- sorter = wilder.python_difflib_sorter()
    }),

    {
      wilder.check(function(ctx, x) return x == '' end),
      wilder.history(15),
    },
    -- search current buffer
    wilder.python_search_pipeline({
      pattern = wilder.python_fuzzy_pattern(),
      -- omit to get results in the order they appear in the buffer
      sorter = wilder.python_difflib_sorter(),
      -- can be set to 're2' for performance, requires pyre2 to be installed
      -- see :h wilder#python_search() for more details
      engine = 're',
    })

  ),
})

wilder.set_option('renderer', wilder.popupmenu_renderer({
  highlighter = wilder.basic_highlighter(),
}))
