vim.g.vimtex_view_method = 'zathura'
-- vim.g.vimtex_latexmk_build_dir = './build'
--

vim.g.vimtex_compiler_latexmk = {
    build_dir = './build',
    options = {
        '-pdf',
        '-shell-escape',
        '-verbose',
        '-file-line-error',
        '-synctex=1',
        '-interaction=nonstopmode',
    }
}
