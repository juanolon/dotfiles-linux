local map = require('utils').map
local telescope = require "telescope"
local actions = require "telescope.actions"

telescope.setup {
    defaults = {
        -- Your defaults config goes in here
        mappings = {
            i = {
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<esc>"] = actions.close,
            },
            n = {
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<esc>"] = actions.close,
            }
        },
        vimgrep_arguments = {
            "rg",
            "--color=never",
            "--no-heading",
            "--with-filename",
            "--line-number",
            "--column",
            "--smart-case"
        },
        previewer = true,
    },
    pickers = {
    },
    extensions = {
        luasnip = {
            preview  = {
                check_mime_type  = true
            },
        },
        fzf = {
            fuzzy = true,                    -- false will only do exact matching
            override_generic_sorter = true,  -- override the generic sorter
            override_file_sorter = true,     -- override the file sorter
            case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
            -- the default case_mode is "smart_case"
        }
    }
}

telescope.load_extension('fzf')
telescope.load_extension('undo')
telescope.load_extension('ui-select')
telescope.load_extension('luasnip')

-- Themes
tele_themes = {}
tele_themes['no_preview'] = function(opts)
    options = require('telescope.themes').get_dropdown({
        layout_config = {
            width = 0.8,
        },
        borderchars = {
            { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
            prompt = {"─", "│", " ", "│", '┌', '┐', "│", "│"},
            results = {"─", "│", "─", "│", "├", "┤", "┘", "└"},
            preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        },
        previewer = false,
        prompt_title = false,
    })

    if opts then options = vim.tbl_extend('force', options, opts) end
    return options
end

tele_themes['default'] = function(opts)
    -- TODO if win-size < someconstant: use dropdown theme, else: use horizontal theme
    options = require('telescope.themes').get_dropdown({
        layout_config = {
            width = 0.8,
            horizontal = {preview_width = 0.6}
        },
        prompt_title = false,
        layout_strategy = "horizontal",
        sort_lastused = true,
        file_ignore_patterns = {"%.git"},
        layout_strategy = "horizontal",
        sorting_strategy = "ascending",
        borderchars = {
            { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
            prompt = {"─", "│", " ", "│", '┌', '┐', "│", "│"},
            results = {"─", "│", "─", "│", "├", "┤", "┘", "└"},
            preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        },
    })

    if opts then options = vim.tbl_extend('force', options, opts) end

    return options
end

tele_themes['cursor'] = function(opts)
    return require('telescope.themes').get_cursor(opts)
end

tele_themes['dropdown'] = function(opts)
    return require('telescope.themes').get_dropdown(opts)
end

-- Openers
local finders = require "telescope.builtin"
function TelescopeOpen(fn, theme)
    opts = {}
    opts['jump_type'] = 'split'

    if tele_themes[theme] ~= nil then
        if fn == "luasnip" then
            telescope.extensions.luasnip.luasnip(tele_themes[theme](opts))
        elseif fn == "undo" then
            telescope.extensions.undo.undo(tele_themes[theme](opts))
        else
            finders[fn](tele_themes[theme](opts))
        end
    else
        finders[fn](opts)
    end
end

