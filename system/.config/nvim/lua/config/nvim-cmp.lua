local cmp = require('cmp')
-- local cmp_autopairs = require('nvim-autopairs.completion.cmp')
-- local snippy = require("snippy")
local ls = require('luasnip')
local compare = cmp.config.compare

local ELLIPSIS_CHAR = '…'
local MAX_LABEL_WIDTH = 20
local MIN_LABEL_WIDTH = 20
local MENU_WIDTH_MAX = 20

-- codicons symbols
local cmp_kinds = {
  Text = '  ',
  Method = '  ',
  Function = '  ',
  Constructor = '  ',
  Field = '  ',
  Variable = '  ',
  Class = '  ',
  Interface = '  ',
  Module = '  ',
  Property = '  ',
  Unit = '  ',
  Value = '  ',
  Enum = '  ',
  Keyword = '  ',
  Snippet = '  ',
  Color = '  ',
  File = '  ',
  Reference = '  ',
  Folder = '  ',
  EnumMember = '  ',
  Constant = '  ',
  Struct = '  ',
  Event = '  ',
  Operator = '  ',
  TypeParameter = '  ',
  Copilot = '  ',
  zk = 'z ',
}

-- local has_words_before = function()
--   local line, col = unpack(vim.api.nvim_win_get_cursor(0))
--   return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
-- end

local select_opts = {behavior = cmp.SelectBehavior.Insert}

-- --[[
--   Get completion context, such as namespace where item is from.
--   Depending on the LSP, this information is stored in different places.
--   The process to find them is very manual: log the payloads And see where useful information is stored.

--   See https://www.reddit.com/r/neovim/comments/128ndxk/comment/jen9444/?utm_source=share&utm_medium=web2x&context=3
-- ]]
-- local function get_lsp_completion_context(completion, source)
--     local ok, source_name = pcall(function() return source.source.client.config.name end)
--     if not ok then return nil end


--     if source_name == "tsserver" then
--         return completion.detail
--     elseif source_name == "pyright" and completion.labelDetails ~= nil then
--         return completion.labelDetails.description
--     elseif source_name == "texlab" then
--         return completion.detail
--     elseif source_name == "clangd" then
--         local doc = completion.documentation
--         if doc == nil then return end

--         local import_str = doc.value

--         local i, j = string.find(import_str, "[\"<].*[\">]")
--         if i == nil then return end

--         return string.sub(import_str, i, j)
--     end
-- end


local mapping_sets = {
        ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "s", "c" }),
        ['<C-u>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "s", "c" }),
        ['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "s", "c" }),
        ['<C-l>'] = cmp.mapping(cmp.mapping.confirm ({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        }), { "i", "s", "c" }),
        ['<C-e>'] = cmp.mapping(cmp.mapping.abort(), { "i", "s", "c" }),
        -- ['<CR>'] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    --
        ["<Tab>"] = cmp.mapping(function(fallback)
            if ls.expand_or_locally_jumpable() then
                ls.expand_or_jump()
            -- elseif has_words_before() then
            --     cmp.complete()
            else
                fallback()
            end
        end, { "i", "s" }),
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if ls.jumpable(-1) then
                ls.jump(-1)
            else
                fallback()
            end
        end, { "i", "s" }),
        ['<C-k>'] = cmp.mapping(function(fallback)
                if cmp.visible() then
                    return cmp.select_prev_item(select_opts)
                end

                fallback()
            end, {"i", "s", "c"}
        ),
        ['<C-j>'] = cmp.mapping(function(fallback)
                if cmp.visible() then
                    return cmp.select_next_item(select_opts)
                end

                fallback()
            end, {"i", "s", "c"}
        ),
    }

cmp.setup({
    snippet = {
        expand = function(args)
            if not ls then
                return
            end
            ls.lsp_expand(args.body)
        end,
    },
    window = {
        -- completion = cmp.config.window.bordered(),
        -- documentation = cmp.config.window.bordered(),
        completion = {
            -- border = { '', '', '', '', '', '', '', '' },
            border = "single",
            -- winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:CursorLineBG,Search:None",
            -- winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None",
            winhighlight = 'NormalFloat:NormalFloat,FloatBorder:FloatBorder',
            -- col_offset = 1,
            col_offset = -5,
            -- scrolloff = 10,
            side_padding = 1,
        },
        documentation = {
            -- border = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
            border = "rounded", -- single|rounded|none
            winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:CursorLineBG,Search:None", -- BorderBG|FloatBorder
        }
    },
    sorting = {
        comparators = {
            compare.score,
            compare.offset,
            compare.recently_used,
            compare.order,
            compare.exact,
            compare.kind,
            compare.locality,
            compare.length,
            -- copied from TJ Devries; cmp-under
            function(entry1, entry2)
                local _, entry1_under = entry1.completion_item.label:find "^_+"
                local _, entry2_under = entry2.completion_item.label:find "^_+"
                entry1_under = entry1_under or 0
                entry2_under = entry2_under or 0
                if entry1_under > entry2_under then
                    return false
                elseif entry1_under < entry2_under then
                    return true
                end
            end,
        },
    },
	mapping = cmp.mapping.preset.insert(mapping_sets),
    sources = cmp.config.sources(
        {
            { name = 'luasnip_choice'},
        },
        {
            { name = 'nvim_lsp' },
            { name = 'luasnip' },
            { name = 'vimtex'},
            { name = 'copilot' },
            -- { name = "otter" },
            -- { name = 'latex_symbols' }
        }, {
            { name = 'buffer' },
            { name = "nvim_lsp_signature_help" },
        }),
    -- performance = {
    --     max_view_entries = 10
    -- }
    completion = {
        -- keyword_length = 1,
        completeopt = "menu,menuone,noselect,preview",
    },
    view = {
        entries = {
            -- name = "native", -- custom
            name = "custom", -- custom
            -- selection_order = 'near_cursor'
        },
    },
    formatting = {
        fields = { "kind", "abbr", "menu" },
        format = function(entry, vim_item)
            local label = vim_item.abbr
            local truncated_label = vim.fn.strcharpart(label, 0, MAX_LABEL_WIDTH)

            if truncated_label ~= label then
                vim_item.abbr = truncated_label .. ELLIPSIS_CHAR
            elseif string.len(label) < MIN_LABEL_WIDTH then
                local padding = string.rep(' ', MIN_LABEL_WIDTH - string.len(label))
                vim_item.abbr = label .. padding
            end

            local kind = vim_item.kind
            -- source name is the client name
            local ok, source_name = pcall(function() return entry.source.source.client.config.name end)
            if source_name == "zk" and kind == "Text" then
                kind = "zk"
            end
            local kind_icon = cmp_kinds[kind] or ''
            -- show [icon] [name] [kind]
            vim_item.kind = " " .. kind_icon .. " "

            -- show the function import as the menu
            -- local cmp_ctx = get_lsp_completion_context(entry.completion_item, entry.source)
            -- if cmp_ctx ~= nil and cmp_ctx ~= "" then
            --     vim_item.menu = cmp_ctx
            -- else
            --     vim_item.menu = ''
            -- end

            vim_item.menu = kind
            -- max length for menu
            local menu_width = string.len(vim_item.menu)
            if menu_width > MENU_WIDTH_MAX then
                vim_item.menu = vim.fn.strcharpart(vim_item.menu, 0, MENU_WIDTH_MAX - 1)
                vim_item.menu = vim_item.menu .. '…'
            else
                local padding = string.rep(' ', MENU_WIDTH_MAX - menu_width)
                vim_item.menu = padding .. vim_item.menu
            end
            return vim_item
        end,
    }
})

-- autopairs disabled for now
-- cmp.event:on(
--   'confirm_done',
--   cmp_autopairs.on_confirm_done()
-- )

-- Set configuration for specific filetype.
-- cmp.setup.filetype('gitcommit', {
--     sources = cmp.config.sources({
--         { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
--     }, {
--         { name = 'buffer' },
--     })
-- })

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline({ '/', '?' }, {
--     mapping = cmp.mapping.preset.cmdline(mapping_sets),
--     view = {
--         entries = {
--             name = "custom",
--             selection_order = 'top_down',
--         }
--     },
--     sources = {
--         { name = 'buffer' }
--     }
-- })

-- cmp.setup.cmdline(':', {
--     -- mapping = cmp.mapping.preset.cmdline(mapping_sets),
--     mapping = cmp.mapping.preset.cmdline(),
--     view = {
--         entries = {
--             name = "custom",
--             selection_order = 'top_down',
--         }
--     },
--     sources = cmp.config.sources({
--         {{ name = 'path' }},
--         {{ name = 'cmdline' }},
--         {{ name = 'cmdline_history' }},
--     }),
-- })
