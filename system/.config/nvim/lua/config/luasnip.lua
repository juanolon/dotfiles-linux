local ls = require"luasnip"

-- vim.cmd [[
--   " Expand
--   imap <silent><expr> <Tab> luasnip#expandable() ? '<Plug>luasnip-expand-snippet' : '<Tab>'

--   " Jump forward
--   imap <silent><expr> jk luasnip#jumpable(1) ? '<Plug>luasnip-jump-next' : 'jk'
--   smap <silent><expr> jk luasnip#jumpable(1) ? '<Plug>luasnip-jump-next' : 'jk'

--   " Jump backward
--   imap <silent><expr> <C-b> luasnip#jumpable(-1) ? '<Plug>luasnip-jump-prev' : '<C-j>'
--   smap <silent><expr> <C-b> luasnip#jumpable(-1) ? '<Plug>luasnip-jump-prev' : '<C-j>'

--   " Cycle forward through choice nodes with Control-F
vim.cmd [[
inoremap <c-u> <cmd>lua require("luasnip.extras.select_choice")()<cr>
imap <silent><expr> <C-w> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-w>'
smap <silent><expr> <C-w> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-w>'
]]

ls.config.set_config({
    -- Don't store snippet history for less overhead
    history = false,
    -- Allow autotrigger snippets
    enable_autosnippets = true,
    update_events = 'TextChanged,TextChangedI',
    -- For equivalent of UltiSnips visual selection
    store_selection_keys = "<Tab>",
    -- Event on which to check for exiting a snippet's region
    region_check_events = 'InsertEnter',
    delete_check_events = 'InsertLeave',
})

require("luasnip.loaders.from_lua").lazy_load({paths = "~/.config/nvim/snippets/"})
-- require("luasnip.loaders.from_vscode").lazy_load()
vim.keymap.set('', '<Leader>U', '<Cmd>lua require("luasnip.loaders.from_lua").lazy_load({paths = "~/.config/nvim/snippets/"})<CR><Cmd>echo "Snippets refreshed!"<CR>')


-- open choices automatically, when a choice is entered
-- vim.api.nvim_create_autocmd("User", {
--         pattern = "LuasnipChoiceNodeEnter",
--         callback = function()
--                 if require("luasnip").choice_active() then
--                         require "luasnip.extras.select_choice"()
--                 end
--                 -- print(require("luasnip").session.event_node)
--                 -- print "hmm"
--         end,
-- })
-- show sign when a choice is availale
vim.fn.sign_define("LuasnipSignChoiceNode", {
    text = "",
    texthl = "LuasnipChoiceNode",
})

local group = vim.api.nvim_create_augroup("config.plugins.luasnip.choice", {})
vim.api.nvim_create_autocmd("User", {
    group = group,
    pattern = "LuasnipChoiceNodeEnter",
    callback = function(a)
        vim.fn.sign_place(
            0,
            "config.plugins.luasnip.choice",
            "LuasnipSignChoiceNode",
            a.buf,
            {
                lnum = vim.api.nvim_win_get_cursor(vim.api.nvim_get_current_win())[1],
            }
        )
    end,
})
vim.api.nvim_create_autocmd("User", {
    group = group,
    pattern = "LuasnipChoiceNodeLeave",
    callback = function(a)
        vim.fn.sign_unplace("config.plugins.luasnip.choice", {
            buffer = a.buf,
        })
    end,
})
