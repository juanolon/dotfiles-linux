local mode = require('treesitter/mathmode')

require('snippy').setup({
    enable_auto = true,
    scopes = {
        _ = { },
        markdown = { '_', 'markdown', 'math' },
        tex = { '_', 'tex', 'math' },
        python = { '_', 'python', 'math' },
    },
    expand_options = {
        m = function()
            return vim.fn["vimtex#syntax#in_mathzone"]() == 1
            -- return mode.in_mathzone()
        end,
        c = function()
            -- return mode.in_comment()
            return vim.fn["vimtex#syntax#in_comment"]() == 1
        end,
    }
})
