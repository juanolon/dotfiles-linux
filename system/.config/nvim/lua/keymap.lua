-- MAPS
local map = require('utils').map

map('c', 'w!!', '%!sudo tee > /dev/null %')
map('c', 'W', 'w')
map('c', 'Bd', 'Bd')

map('n', 'k', 'gk')
map('n', 'j', 'gj')
map('n', '0', 'g0')
map('n', '$', 'g$')

map('n', '<c-j>', '4j')
map('n', '<c-k>', '4k')
map('c', '<c-j>', '4j')
map('c', '<c-k>', '4k')

map('n', '<C-h>', '^')
map('n', '<C-l>', '$')
map('v', '<C-h>', '^')
map('v', '<C-l>', '$')

map('n', 'VV', 'V')
map('n', 'Vit', 'vitVkoj')
map('n', 'Vat', 'vatV ')
map('n', 'Vab', 'vabV ')
map('n', 'VaB', 'vaBV ')

map('c', '<C-b>', '<S-Left>')
map('c', '<C-w>', '<S-Right>')
map('c', '<C-j>', '<Down>')
map('c', '<C-k>', '<Up>')
map('c', '<C-h>', '<Left>')
map('c', '<C-l>', '<Right>')
map('c', '<C-d>', '<C-w>')

map('n', 'gp', '`[v`]')

map('n', '<C-n>', 'gt')
map('n', '<C-p>', 'gT')

map('n', '<C-w>O', '<C-w>T')

map("v", "<", "<gv")
map("v", ">", ">gv")

-- LSP
-- vim.api.nvim_create_autocmd('LspAttach', {
--   group = vim.api.nvim_create_augroup('UserLspConfig', {}),
--   callback = function(ev)
--     -- Enable completion triggered by <c-x><c-o>
--     vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'
--
--     -- vim.keymap.set('n', 'gD', "<cmd>Lspsaga lsp_finder<CR>", bopts)
--     -- -- tab split | Lspsaga goto_definition
--     -- vim.keymap.set('n', 'gd', "<cmd>Lspsaga peek_definition<CR>", bopts)
--     -- vim.keymap.set('n', 'go', "<cmd>Lspsaga outgoing_calls<CR>", opts)
--     -- vim.keymap.set('n', 'gi', "<cmd>Lspsaga incoming_calls<CR>", opts)
--
--     -- -- vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
--
--     -- vim.keymap.set('n', '<leader>ca', "<cmd>Lspsaga code_action<CR>", bopts)
--
--     -- vim.keymap.set('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', bopts)
--     -- vim.keymap.set('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', bopts)
--     -- vim.keymap.set('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', bopts)
--     -- vim.keymap.set('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', bopts)
--     -- Buffer local mappings.
--     -- See `:help vim.lsp.*` for documentation on any of the below functions
--     local opts = { buffer = ev.buf }
--     vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
--     -- vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
--     vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
--     vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
--     vim.keymap.set('n', '<space>wl', function()
--       print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
--     end, opts)
--     vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
--     vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
--     vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts)
--
--     -- vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
--     vim.keymap.set('n', 'gi', ":lua TelescopeOpen('lsp_implementations', 'cursor')<CR>", opts)
--     vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
--     -- vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
--     vim.keymap.set('n', 'gd', ":lua TelescopeOpen('lsp_definitions', 'cursor')<CR>", opts)
--     -- vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
--     vim.keymap.set('n', 'gr', ":lua TelescopeOpen('lsp_references', 'cursor')<CR>", opts)
--
--     vim.keymap.set('n', '<space>f', function()
--       vim.lsp.buf.format { async = true }
--     end, opts)
--
--   end,
-- })

-- global mapping
-- set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
local silent = { silent = true }
local opts = { noremap=true, silent=true }
-- vim.keymap.set('n', '<space>q', vim.diagnostic.setqflist, opts)
-- vim.keymap.set("n", "ge", "<cmd>lua vim.diagnostic.open_float({ border = 'rounded', max_width = 100 })<CR>", silent)
-- -- vim.keymap.set("n", "L", "<cmd>lua vim.lsp.buf.signature_help()<CR>", silent)
-- vim.keymap.set("n", "<Down>", "<cmd>lua vim.diagnostic.goto_next({ float = { border = 'rounded', max_width = 100 }})<CR>", silent)
-- vim.keymap.set("n", "<Up>", "<cmd>lua vim.diagnostic.goto_prev({ float = { border = 'rounded', max_width = 100 }})<CR>", silent)
-- Notebook

-- generate documentation
-- map('n', '<Leader>cc', ":lua require('neogen').generate({ type = 'class' })<CR>")
-- map('n', '<Leader>cf', ":lua require('neogen').generate({ type = 'func' })<CR>")
-- map('n', '<Leader>ct', ":lua require('neogen').generate({ type = 'type' })<CR>")

-- jump to next highlight word
-- vim.keymap.set('n', '<Tab>', ':lua require("illuminate").goto_next_reference()<cr>')
-- vim.keymap.set('n', '<S-Tab>', ':lua require("illuminate").goto_prev_reference()<cr>')

-- Telescope
-- local opts = {noremap = true, silent = true, expr = false}
-- map('n', '<C-b>', ":lua TelescopeOpen('buffers', 'no_preview')<CR>", opts)
-- map('n', '<C-t>', ":lua TelescopeOpen('find_files', 'default')<CR>", opts)
-- map('n', '<C-s>', ":lua TelescopeOpen('live_grep', 'default')<CR>", opts)
--
-- map('n', '<leader>u', ":lua TelescopeOpen('undo', 'default')<CR>", opts)
-- map('n', '<leader>s', ":lua TelescopeOpen('luasnip', 'dropdown')<CR>", opts)

-- TODO
-- map('n', '<leader>u', ":lua require('telescope').load_extension('zk')<CR>", opts)

-- img-clip
-- map('n', '<leader>p', ":lua require('img-clip').paste_image({relative_to_current_file= true, dir_path='.'})<CR>", opts)
