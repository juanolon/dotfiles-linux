local ls = require("luasnip")
local u = require('utils')

local decorator = {
    snippetType="autosnippet",
    condition = u.pipe({ u.in_mathzone }),
}

local s = ls.extend_decorator.apply(ls.snippet, decorator)

return
    {
        s({ trig = "Ex", name = "Expectation" }, {
            t("\\mathbb{E}")
        }),
        s({ trig = "OO", name = "emptyset"}, {
            t("\\O"),
        }),
        s({ trig = "RR", name = "R" }, {
            t("\\mathbb{R}")
        }),
        s({ trig = "QQ", name = "Q" }, {
            t("\\mathbb{Q}")
        }),
        s({ trig = "ZZ", name = "Z" }, {
            t("\\mathbb{Z}")
        }),
        s({ trig = "UU", name = "cup" }, {
            t("\\cup ")
        }),
        s({ trig = "NN", name = "n" }, {
            t("\\mathbb{N}")
        }),
        s({ trig = "||", name = "mid" }, {
            t(" \\mid ")
        }),
        s({ trig = "Nn", name = "cap" }, {
            t("\\cap ")
        }),
        s({ trig = "bmat", name = "bmat" }, {
            t("\\begin{bmatrix} $1 \\end{bmatrix} $0")
        }),
        s({ trig = "uuu", name = "bigcup" }, {
            t("\\bigcup_{${1:i \\in ${2: I}}} $0")
        }),
        s({ trig = "DD", name = "D" }, {
            t("\\mathbb{D}")
        }),
        s({ trig = "HH", name = "H" }, {
            t("\\mathbb{H}")
        }),
        s({ trig = "lll", name = "l" }, {
            t("\\ell")
        }),

    }
