---
date: {{format-date now "%m-%d-%Y"}}
title: {{title}}
courses: {{dir}}
tags: []
---
{{content}}
