using LanguageServer, Sockets, SymbolServer;
using Pkg;
import StaticLint;
import SymbolServer;

env_path = dirname(Pkg.Types.Context().env.project_file);
debug = true;

@info "Starting Julia Language Server on $env_path"

server = LanguageServerInstance(stdin, stdout, debug, env_path, "");
server.runlinter = true;
run(server);


# using LanguageServer;
# server = LanguageServer.LanguageServerInstance(isdefined(Base, :stdin) ? stdin : STDIN, isdefined(Base, :stdout) ? stdout : STDOUT, false);
# server.runlinter = true;
# run(server);
