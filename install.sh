#!/usr/bin/zsh

# last git version
sudo apt-add-repository -y ppa:git-core/ppa

# system
# if it is commented, then may not be necessary
sudo apt install /
    wicd-curses /
    git-core /
    tmux /
    build-essential /
    golang /
    stow /
    tig /
    pass /
    ranger /
    zsh /
    pavucontrol /
    rofi /
    pulseaudio /
    pulseaudio-module-bluetooth /
    pulseaudio-utils /
    chromium /
    "feh" /
    scrot /
    dunst /
    xcape /
    xinit /
    silversearcher-ag /
    colordiff /
    freetype /
    fonts-materialdesignicons-webfont /
    xbacklight /
    dirmngr /
    software-properties-common /
    fasd /
    cmake /
    python-dev python3-dev /
    cups cups-daemon / # printer
    sane-utils / # scanner
    afew /
    unclutter /
    libnotify-bin /
    curl /
    dbus-x11 /
    xinput /
    xdotool /
    zenity /
    fzf
    # printer-driver-escpr # printer drivers
    # pdfgrep /
    # graphviz /
    # valgrind

# applications
sudo apt install /
    vlc /
    rtorrent

    # pulseaudio-module-x11
    # pulseaudio-utils

# apt sources
/etc/apt/sources.list
    <url> testing main non-free contrib
    <url> unstable main non-free contrib

/etc/apt/preferences.d/unstable
    Package: *
    Pin: release a=stable
    Pin-priority: 700

    Package: *
    Pin: release a=testing
    Pin-Priority: 650

    Package: *
    Pin: release a=unstable
    Pin-Priority: 600

su
/usr/sbin/adduser juanolon sudo

# st
sudo apt install /
    libfontconfig1-dev /
    libfreetype6-dev /
    libx11-dev /
    libxft-dev

# zsh
git clone --recursive git@bitbucket.org:juanolon/prezto.git .zprezto
setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done
chsh -s /usr/bin/zsh

# st (simple terminal)
git clone git://git.suckless.org/st
copy st_config.h
# sudo make install

# neovim
apt install neovim python3-dev python3-pip
pip3 install neovim
# install PLUG
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
# install dependencies for plugins..
sudo apt install nodejs npm

# pass
pass init "juanolon@gmail.com"
pass git init

pass git remote add origin git@bitbucket.org:juanolon/pass.git
pass git pull origin master
https://github.com/carnager/rofi-pass

# astroid
# sudo apt install meson

# dotfiles
stow -t $HOME system
stow -t $HOME i3
stow -t $HOME dev
stow -t $HOME apps

# submodules
# tmux -> .config/tmux/...
# git clone --depth 1 https://github.com/woodstok/tmux-butler ~/.config/tmux/tmux-butler

# fonts - delete cache
# fc-cache -f -v
# apt install fonts-font-awesome


# astroid
#
#
# required manual setup
echo "password" | gpg2 --encrypt --recipient juanolon@gmail.com -o mail/keys/juan.stumpf@student.uibk.ac.at

# replace auto with allow-hotplug on /etc/network/interfaces for faster boot time


# ANDROID
# download sdk (command line tools)
http://developer.android.com/sdk/index.html
# and unpack it on .android/tools
.android/tools/bin/sdkmanager --update
# this versions are required for react-native..
.android/tools/bin/sdkmanager "platforms;android-23" "build-tools;23.0.1" "add-ons;addon-google_apis-google-23"
.android/tools/bin/sdkmanager --licenses

# cgdb
$ git clone git://github.com/cgdb/cgdb.git
$ cd cgdb
$ ./autogen.sh
$ ./configure --prefix=/usr/local
$ make
$ sudo make install

Anyconnect VPN
sudo apt install openconnect
sudo openconnect vpn.uibk.ac.at -ucsav4859

# required repositories ~/repos
git@github.com:geometry-zsh/geometry
git@github.com:jaagr/polybar.git
git@github.com:carnager/teiler.git
# teiler requirements: slop maim
# add video user, and give permissions to change the backlight
# https://wiki.archlinux.org/index.php/Backlight#ACPI
# /etc/udev/rules.d/backlight.rules
# > ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
# > ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"

# stack - haskell
curl -sSL https://get.haskellstack.org/ | sh
git clone https://github.com/haskell/haskell-ide-engine --recursive
cd haskell-ide-engine
stack --stack-yaml=stack-8.2.2.yaml install

stack install funnyprint hlint hoogle

# vim binaries
# c/c++ autocomplete
git clone --depth=1 --recursive https://github.com/MaskRay/ccls
sudo apt install clang cmake libclang-dev llvm-dev rapidjson-dev
cmake -H. -BRelease
cmake --build Release

# python autocomplete
# pip3 install python-language-server
#
#
# go-langserver
go get -u github.com/sourcegraph/go-langserver

# latex
sudo apt install texlive-latex-base texlive-latex-extra texlive-latex-recommended
texlab

## in case of error:
# upgrade https://tug.org/texlive/quickinstall.html
# #### install small scheme: To install a smaller scheme, pass --scheme=scheme to install-tl. For example, --scheme=small corresponds to the BasicTeX variant of MacTeX. 

# create tlmgr env:
tlmgr init-usertree
# installing fonts: tlmgr install bera # uibk font
# searching fonts: tlmgr info berasans
# and updating: updmap -user
#
# https://sw.kovidgoyal.net/kitty/index.html
# to print images on terminal:
# pip3 install ueberzug
#
# spotify
# server: https://github.com/Spotifyd/spotifyd
# mpris client: apt install playerctl
#
#
#-rwxr-xr-x 1 root     root      28M Oct 23 01:04 ccls
-rwxr-xr-x 1 root     root      23M Oct 23 01:04 ccls.bak
-rwxr-xr-x 1 root     root     2.3M Oct 13  2019 cgdb
lrwxrwxrwx 1 root     root       24 Mar 28 00:25 gpuverify -> /opt/GPUVerify/gpuverify
lrwxrwxrwx 1 root     root       26 Mar  7 03:09 julia -> /opt/julia-1.3.1/bin/julia
-rwxr-xr-x 1 root     root      16M Jan  5 20:15 nvim
-rwxr-xr-x 1 root     root     3.5M Oct 13  2019 polybar
-rwxr-xr-x 1 root     root     101K Oct 13  2019 polybar-msg
-rwxr-xr-x 1 root     root     1.5K Oct 13  2019 screen-backlight
-rwxr-xr-x 1 juanolon juanolon  18M Jan 24 20:29 spotifyd
-rwxr-xr-x 1 root     root      97K Oct 12  2019 st

# ipad screen sharing
# https://github.com/antimof/UxPlay
# sudo apt-get install libssl-dev libavahi-compat-libdnssd-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-libav
# apt install gstreamer1.0-plugins-bad
#



#########
# New setup.
# Debian 12.2. wayland
#

# set user to sudo group
su -
apt install sudo
usermod -a -G sudo juanolon

# install windows manager
sudo apt install waybar sway neovim kitty firefox-esr swaylock zsh wl-clipboard

################
# setup default shell
chsh -s /usr/bin/zsh

# install prompt
# TODO check in zshrc if binary exist, otherwise use default prompt
cargo install starship --locked


################
# install software
#
# install basic tools
sudo apt install git tig pass ranger zsh pavucontrol curl cmake fonts-font-awesome jq wayland-utils alsa-utils bc acpi thunar transmission unzip zathura htop ripgrep sudo curl swaybg silversearcher-ag ripgrep fd-find zip rfkill dunst pinentry-qt gnupg2 rsync colordiff libnotify-bin bat

# installed cli:
# ag: search for files
# rg: search for files. replacement for ag
# fd: search entries in file system. aka file names
# curl

# install libs
build-essential zlib1g-dev libffi-dev libssl-dev libbz2-dev libreadline-dev libsqlite3-dev liblzma-dev

# install pip
sudo apt install python3-pip

# setup pyenv
curl https://pyenv.run | bash
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo '[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zshrc

# setup pipenv
pip install pipenv virtualenv


# image viewer
# https://github.com/woelper/oculante/releases
# sudo dpkg -i ~/Downloads/oculante-0.7.7.deb

# kitty
# DONT INSTALL IT LIKE THIS. IT BREAKS THE SYSTEM
# https://github.com/kovidgoyal/kitty/releases/latest
# mv kitty-....txz kittty
# unarchive kitty-...
# sudo cp -r share/ /usr/local/
# sudo cp -r lib/ /usr/local
# sudo cp -r bin/ /usr/local
# INSTEAD, USE INSTALL SCRIPT:
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
cd ~/.bin
ln -s /home/juanolon/.local/kitty.app/bin/kitty kitty
ln -s /home/juanolon/.local/kitty.app/bin/kitten kitten

# neovim
# https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
# tar xzvf nvim-linux64.tar.gz
# sudo cp -r share/ /usr/local
# sudo cp -r man/man1 /usr/local/man
# sudo cp -r lib/ /usr/local/
# sudo cp -r bin/ /usr/local/
# pip install --upgrade pynvim
# install extra font:
# copy ttf file to ~/.fonts. then execute fc-cache
# font
#   https://github.com/vercel/geist-font/releases/tag/1.3.0
# icons:
#   https://microsoft.github.io/vscode-codicons/dist/codicon.html
#   https://github.com/microsoft/vscode-codicons/releases/download/latest/codicon.ttf
#   https://phosphoricons.com (https://github.com/phosphor-icons/web/blob/master/src/regular/Phosphor.ttf)

# setup screeshots / lock screen
sudo apt install grim slurp imagemagick
# deps for grimshot. fork of grimshot is located in .bin/grimshot
# wl-copy, notyfi-send, swaymsg are binaries, no repositories
sudo apt install wl-copy jq notyfi-send swaymsg slurp grim

sudo apt install software-properties-common python3-launchpadlib # -> add-apt-repository

# ###############
# setup interception. enable remapping capslock to esc+ctrl
sudo add-apt-repository ppa:deafmute/interception
sudo apt install interception-tools

# change to older repository:
juanolon@home: cat /etc/apt/sources.list.d/deafmute-ubuntu-interception-bookworm.list
deb https://ppa.launchpadcontent.net/deafmute/interception/ubuntu/ bionic main
# deb-src https://ppa.launchpadcontent.net/deafmute/interception/ubuntu/ bionic main

sudo apt install libevdev-dev libyaml-cpp-dev
git clone https://gitlab.com/interception/linux/plugins/dual-function-keys.git
cd dual-function-keys
make && sudo make install

# if config not working, try fixing the spaces/tabs of the yaml files

juanolon@home:~$ cat /etc/interception/udevmon.yaml
- JOB: "interception -g $DEVNODE | dual-function-keys -c /etc/interception/dual-function-keys/my-mappings.yaml | uinput -d $DEVNODE"
  DEVICE:
    NAME: "AT Translated Set 2 keyboard"

▲ ~ cat /etc/interception/dual-function-keys/my-mappings.yaml
MAPPINGS:
  - KEY: KEY_LEFTSHIFT
    TAP: [KEY_LEFTSHIFT, KEY_9]
    HOLD: KEY_LEFTSHIFT
  - KEY: KEY_RIGHTSHIFT
    TAP: [KEY_LEFTSHIFT, KEY_0]
    HOLD: KEY_RIGHTSHIFT
  - KEY: KEY_CAPSLOCK
    TAP: KEY_ESC
    HOLD: KEY_LEFTCTRL

juanolon@home:~$ cat /etc/systemd/udevmon.service
 [Unit]
 Description=udevmon
 Wants=systemd-udev-settle.service
 After=systemd-udev-settle.service

 [Service]
 ExecStart=/usr/bin/nice -n -20 /usr/bin/udevmon -c /etc/udevmon.yaml

 [Install]
 WantedBy=multi-user.target

sudo systemctl enable --now udevmon

################
# setup internet

# install network manager
# as long the iface entry in /etc/network/interface exist, nm will not manage that entry
# remove it from the interface file, and restart service or computer
sudo apt install network-manager network-manager-gnome
nm-applet --indicator # systray icon
nm-connection-editor # 

# change interfaces with wlan access. then reload networking
juanolon@home:~$ sudo cat /etc/network/interfaces
source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug wlp4s0
iface wlp4s0 inet dhcp
	wpa-ssid chillerweg 2.0
	wpa-psk  goGreen4F420

## change last lines to this:
# The primary network interface
# allow-hotplug wlp4s0
# iface wlp4s0 inet dhcp
# 	wpa-ssid web
# 	wpa-psk  webwebweb

# reload config (or restart system)
sudo ifdown/ifup wlp4s0
sudo iw wlp4s0 link
sudo ip a


################
# setup bluetooth
sudo apt install pulseaudio-module-bluetooth
sudo apt install blueman # ui

# setup docker
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
# sudo install -m 0755 -d /etc/apt/keyrings
# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
# sudo chmod a+r /etc/apt/keyrings/docker.gpg

# # Add the repository to Apt sources:
# echo \
#   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
#   "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
#   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# sudo apt-get update

# sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# #######
# install albert
echo 'deb http://download.opensuse.org/repositories/home:/manuelschneid3r/Debian_12/ /' | sudo tee /etc/apt/sources.list.d/home:manuelschneid3r.list
curl -fsSL https://download.opensuse.org/repositories/home:manuelschneid3r/Debian_12/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_manuelschneid3r.gpg > /dev/null
sudo apt update
sudo apt install albert

# #######
# install brightness control:
sudo apt install brightnessctl

# gamma/temperature
git clone https://github.com/MaxVerevkin/wl-gammarelay-rs
cargo install wl-gammarelay-rs --locked
mv /home/juanolon/.cargo/bin/wl-gammarelay-rs /usr/local/bin

# #######
# install spotify
curl -sS https://download.spotify.com/debian/pubkey_7A3A762FAFD4A51F.gpg | sudo gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
# ensure that xdg-desktop-menu entry is writen. if folder doesn't exist, it doesnt make an entry
sudo mkdir /usr/share/desktop-directories
sudo apt-get update && sudo apt-get install spotify-client

# install java
sudo apt install default-jdk

# udiskie
sudo apt install udiskie

# gparted
sudo apt install gparted
# for FAT tools
sudo apt install dosfstools mtools
# execute this to start gparted on wayland
xhost +SI:localuser:root

# for Xserver apps
sudo apt install xwayland

# latex
# https://yihui.org/tinytex/
# to find missing packages:
# TODO make script
# tlmgr search --global --file <file.sty>
# tlmgr install <package>
# tlmgr path add
# find out installed file:
# kpsewhich beamer.cls
# tlmgr install grfext setspace beamer csquotes

# last version suported by jupyter
pip install jupyter-ascending # will also install the correct notebook
# pip install notebook==6.4.13
pip install nbdime

# flutter
# download flutter: https://docs.flutter.dev/get-started/install/linux
# tar xf ~/Downloads/flutter_linux_3.13.9-stable.tar.xz
# move flutter to ~/.fluttersdk
# sudo apt install clang


# screen-backlight sudoers:
sudo cp .bin/screen-backlight /usr/local/bin/screen-backlight
sudo visudo # sudo nvim /etc/sudoers
juanolon ALL=NOPASSWD:/usr/local/bin/screen-backlight

# GPG
# test gpg:
# encript:  gpg --encrypt --output test.gpg --recipient juanolon@gmail.com test.py
# decript:  gpg  -vvv --decrypt --output test.dec test.gpg
# restart:  gpg-connect-agent updatestartuptty /bye
# reload:   gpg-connect-agent reloadagent /bye
#
# error:
# ~ pass -c acc/steampowered.com
# gpg: decryption failed: No secret key
# because pinentry was not working. config on .gnupg/gpg-agent.conf
# ~ cat .gnupg/gpg-agent.conf 
# pinentry-program /usr/bin/pinentry-x11


# steam:
# sudo dpkg --print-architecture
# sudo dpkg --add-architecture i386
# sudo apt install wget gdebi-core libgl1-mesa-glx:i386
# install dpkg -i steam.deb


# set default file
# mimeopen -d image.png
# open with default: xdg-open image.png or mimeopen image.png

#install npm
sudo apt install nodejs npm
#
# install wluma
https://github.com/maximbaz/wluma?tab=readme-ov-file
sudo apt-get -y install v4l-utils libv4l-dev libudev-dev libvulkan-dev libdbus-1-dev libclang-dev
make build
sudo make install

# zk
https://github.com/zk-org/zk/releases

# fzf
https://github.com/junegunn/fzf

# git with fzf
# https://github.com/wfxr/forgit
# https://github.com/bigH/git-fuzzy

# zsh jump
https://github.com/gsamokovarov/jump

# hugo
# https://github.com/gohugoio/hugo/releases

# pomodoro waybar:
https://github.com/jkallio/pomodoro-cli

# xlog
gihub.com

# colorscheme
# https://rosepinetheme.com/

# analyse colors
# wget "https://github.com/sharkdp/pastel/releases/download/v0.8.1/pastel_0.8.1_amd64.deb"
# sudo dpkg -i pastel_0.8.1_amd64.deb
# $ grep -o "#[a-f0-9]\{6\}" ~/.config/kitty/current-theme.conf | pastel color

# setup theme
# download rose theme gtk3 and unarchive to ~/.themes
# https://github.com/rose-pine/gtk/releases/tag/v2.1.0
# downlaoad all icons from the release page to ~/.local/share/icons/

# change LS_COLORS
# LS_COLORS are generated using the tool vivid:
# the generated string is saved into .config/zsh/lscolors, which is loaded
# in current-lscolors.zsh.
# only install vivid to generate new colors:
# get vivid: https://github.com/sharkdp/vivid
# generate LS_COLORS string with: (rose-pine-dawn.yml is in vivid-themes folder)
vivid generate rose-pine-dawn.yml | wl-copy
# and paste the string into a theme file: .config/zsh/lscolors/<theme>.zsh

# add theme to batcat
# url: https://github.com/rose-pine/tm-theme
mkdir -p "$(bat --config-dir)/themes"
cd "$(bat --config-dir)/themes"
curl --remote-name-all https://raw.githubusercontent.com/rose-pine/tm-theme/main/dist/themes/rose-pine{,-dawn,-moon}.tmTheme
bat cache --build
echo '--theme="rose-pine"' >> "$(bat --config-file)"

# anki
# download anki-24.04.1-linux-qt6.tar.zst
# unzip
# sudo ./install.sh
# plugin for markdown
# https://ankiweb.net/shared/info/822767335

# battery performance while charging
sudo apt install tlp tlp-rdw smartmontools

# qutebrowser
sudo apt install qutebrowser libjs-pdf

# better top
sudo apt install btop

# a better diff
sudo apt install difftastic

# git ui
# https://github.com/extrawurst/gitui/releases

# yazi: file explorer (instead of ranger)
# https://github.com/sxyazi/yazi/releases

# install imagemagick
git clone https://github.com/ImageMagick/ImageMagick.git
sudo apt install build-essential libltdl-dev libjpeg-dev libpng-dev libtiff-dev libgif-dev libfreetype6-dev liblcms2-dev libxml2-dev
./configure --with-modules
make
sudo make install
sudo ldconfig /usr/local/lib
magick --version

# screen sharing
sudo apt install xdg-desktop-portal-wlr pipewire pipewire-media-session-pulseaudio pipewire-pulse
# in case audio is gone, restart the service:
# systemctl --user restart pipewire.service

# image viewer
sudo apt install imv

# eww
# dependencies:
sudo apt install libgtk-3-dev libgio2.0-cil-dev libdbusmenu-gtk3-dev libgtk-layer-shell-dev

git clone --depth 1 https://github.com/elkowar/eww.git
cargo build --release --no-default-features --features=wayland

# Applications:
# nemo
# vlc
# qdirstat
# anki
# qbittorrent
# playerctl
# gucharmap
# wluma???? or delete!

# DPKG -i
# zoom:
#   https://zoom.us/download
# discord:
#   https://discord.com/download
# pastel:
#   wget "https://github.com/sharkdp/pastel/releases/download/v0.8.1/pastel_0.8.1_amd64.deb"
# oculante:
#   https://github.com/woelper/oculante/releases/latest
